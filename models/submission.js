"use strict";

module.exports = function(sequelize, DataTypes) {
    var Submission = sequelize.define("Submission", {
            ExperimentId: DataTypes.STRING,
            ChosenQuestionId: DataTypes.INTEGER,
            SubjectId: DataTypes.INTEGER,
            Answer: DataTypes.STRING,
            IgnoredQuestionId: DataTypes.INTEGER,
            UserId: DataTypes.INTEGER,
            time: DataTypes.INTEGER,
            grade: DataTypes.INTEGER
        }
    , {
        classMethods: {
        }
    });

    return Submission;
};