var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongo = require('./mongo/models')


var routes = require('./routes/index');
var users = require('./routes/users');
var quizzes = require('./routes/quizzes');
var submissions = require('./routes/submissions');
var oauth = require('./routes/oauth');
var logout = require('./routes/logout');
var login = require('./routes/login');
var resubmit = require('./routes/resubmit');
var stats = require('./routes/stats');
var experiment_submit = require('./routes/experiment_submit');
var experiments_data = require('./routes/experiments_data');
var experiments_user = require('./routes/experiments_user');
var experiments_quizzes = require('./routes/experiments_quizzes');
var experiments_stats = require('./routes/experiments_stats');

var quizzes_canvas = require('./routes/quizzes_canvas');
var submissions_canvas = require('./routes/submissions_canvas');
var submissions_attempts = require('./routes/submissions_attempts');
var quiz_submit = require('./routes/quiz_submit');
var quiz_start = require('./routes/quiz_start');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));



app.use('/', routes);
app.use('/users', users);
app.use('/quizzes', quizzes);
app.use('/submissions', submissions);
app.use('/oauth2response', oauth);
app.use('/logout', logout);
app.use('/login', login);
app.use('/resubmit', resubmit);
app.use('/stats', stats);
app.use('/experiment_submit', experiment_submit);
app.use('/experiments_data', experiments_data);
app.use('/experiments_user', experiments_user);
app.use('/experiments_quizzes', experiments_quizzes);
app.use('/experiments_stats', experiments_stats);

app.use('/quizzes_canvas', quizzes_canvas);
app.use('/submissions_canvas', submissions_canvas);
app.use('/submissions_attempts', submissions_attempts);
app.use('/quiz_submit', quiz_submit);
app.use('/quiz_start', quiz_start);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
