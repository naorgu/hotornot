/**
 * Created by Amir on 01/04/2015.
 */
HotOrNot.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1

    //
    // Now set up the states
    $stateProvider
        .state('HotOrNot', {
            url: "/HotOrNot?experimentId",
            templateUrl: "HotOrNot.html",
            controller: "QuestionDisplay"
        })
        .state('statistics', {
            url: "/Statistics",
            templateUrl: "Statistics.html",
            controller: "statistics"
        })
        .state('Stat', {
            url: "/Stat",
            templateUrl: "Stat.html",
            controller: "statistics2"
        })
        .state('CreateExp', {
            url: "/CreateExp",
            templateUrl: "CreateExp.html",
            controller: "CreateExp"
        })
        .state('Home', {
            url: "/Home",
            templateUrl: "Home.html"
        })

        .state('AboutUs', {
            url: "/AboutUs",
            templateUrl: "AboutUs.html"
        })
    $urlRouterProvider.otherwise("Home");
});