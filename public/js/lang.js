/**
 * Created by Amir on 01/04/2015.
 */
HotOrNot.config(function ($translateProvider) {
    $translateProvider.translations('en', {
        Language: "עברית",
        Home: 'Home',
        AboutUs: 'About us',
        CreateExp: 'Create Experiment',
        Statistics: 'Statistics',
        MyCourses: 'My Courses',
        SignUp: "Sign Up",
        Login: "Login",
        HomeTitle: "Welcome to HotOrNot website",
        HomeDes: "please choose class and start answering questions",
        AboutUsTitle: "AboutUs",
        AboutUsDes: "about us ...",
        HotOrNotTitle: "HotOrNot",
        HotOrNotDes: "Click on one of the question and answer it!",
        Answer: "Answer",
        Reset: "Reset",
        QuestionTitle: "Question",
        Comparison: "Comparison"

    })
        .translations('he', {
            Language: "english",
            Home: 'בית',
            AboutUs: 'מי אנחנו',
            CreateExp: 'יצירת ניסוי',
            MyCourses: 'התרגולים שלי',
            Statistics: 'סטטיסטיקה',
            SignUp: "הרשמה",
            Login: "התחברות",
            HomeTitle: "ברוכים הבאים!",
            HomeDes: 'תודה על ההשתתפות שלכם בתרגול האינטראקטיבי שלפניכם. אנא בצעו את ״תרגול א״ ואת ״תרגול ב׳״.',
            AboutUsTitle: "מי אנחנו?",
            AboutUsDes: "מידע על גבינו",
            HotOrNotTitle: "להיט או לא",
            HotOrNotDes: "לחץ על אחת מן השאלות וענה עליה!",
            Answer: "ענה",
            Reset: "אפס",
            QuestionTitle: "שאלה לבחירה",
            Comparison: "השוואה"
        });
    $translateProvider.preferredLanguage('he');
});
