/**
 * Created by Amir on 30/05/2015.
 */
HotOrNot.directive('questionPanel',function () {
    return {
        templateUrl:"../html/Question.html",
        restrict: 'E',
        scope: {
            question: '=',
            submit: '&'
        }
    }
});