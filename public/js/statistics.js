/**
 * Created by Amir on 05/05/2015.
 */
HotOrNot.controller('statistics', function ($scope, $http) {
    $http.get('/experiments_user').success(function (data) {
        //$http.get('../MyExp.json').success(function (data) {

        $scope.experiments = data.experiments;
    });

    $scope.selectedExp = [];
    $scope.selectedExpShown = [];

    $scope.settings = {
        externalIdProp: '',
        idProp: 'id',
        displayProp: "name",
        showUncheckAll: false,

        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };


    $scope.loadData = function () {
        $scope.data = [];
        $scope.labels = [];
        $scope.series = [];
        for (var i = 0; i < $scope.selectedExp.length; i++) {
            getData($scope.selectedExp[i].id, $scope.selectedExp[i].name, i === 0);
            $scope.selectedExpShown[i] = $scope.selectedExp[i].name;
        }
        console.log($scope.series)

    };

    $scope.showGraph = function (user) {
        $scope.userDetails = user;
        //console.log($scope.labels);
        //console.log($scope.userDetails);
    }

    function getData(id, name, first) {

        $http.get('/Stats?experimentId=' + id).success(function (data) {
            $scope.series[$scope.series.length] = name;
            var tempArr = [];
            if ($scope.labels.length === 0) {
                for (var i = 0; i < data.stats.length; i++) {
                    $scope.labels[i] = "userId: " + data.stats[i].userId;
                }
            }
            for (var i = 0; i < data.stats.length; i++) {
                tempArr[tempArr.length] = data.stats[i].grade;
            }
            $scope.data[$scope.data.length] = tempArr;
            //$scope.usersData[0] = [];
            //for (var i = 0; i < data.userData.length; i++) {
            //    if (first === true) {
            //        $scope.usersName[i] = {
            //            "id": data.userData[i].userId,
            //            "name": data.userData[i].userName,
            //            "data": []
            //        };
            //        $scope.usersName[i].data = data.userData[i].score;
            //
            //    } else {
            //        for (var j = 0; j < $scope.usersName.length; j++) {
            //            if ($scope.usersName[j].id === data.userData[i].userId) {
            //                $scope.usersName[j].data = [$scope.usersName[j].data, data.userData[i].score];
            //                console.log($scope.usersName[i])
            //
            //            }
            //
            //        }
            //    }
            //$scope.usersName[i].data = data.userData[i].score;
        })
    }
})


//HotOrNot.controller('statistics2', function ($scope, $http) {
//    $scope.statisticsData = null;
//    $http.get('/experiment_stats').success(function(data) {
//        $scope.statisticsData=data;
//
//    })
//});