var db = require('../db')
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    db.getUserByToken(req.cookies.auth)
        .then(function (user) {
            if (user != null) {
                var userId = user['id'];
                db.removeQuestionsAnswered(userId)
                    .then(function () {
                        res.redirect('quizzes');
                    })
            } else {
                res.redirect('quizzes');
            }
        });
});


module.exports = router;
