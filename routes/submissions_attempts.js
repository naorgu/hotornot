/**
 * Created by naorguetta on 11/4/14.
 */
var db = require('../db')
var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var canvas = require("../canvas");

router.post('/', function (req, res, next) {
    var token = req.cookies.auth;
    var body = req.body;
    var chosenQuestionId = body.chosen_question_id;
    var chosenAssQuestionId = body.chosen_assessment_question_id;
    var unchosenAssQuestionId = body.unchosen_assessment_question_id;
    var answerId = body.answer;
    var experimentId = body.experimentId;
    var time = body.time;

    insertAttempt(token, experimentId, chosenQuestionId, chosenAssQuestionId, unchosenAssQuestionId, answerId, time)
        .then(function (grade) {
            if (grade == 100) {
                res.json({feedback: true});
            }
            res.json({feedback: false});
        });
});

function insertAttempt(token, experimentId, chosenQuestionId, chosenAssQuestionId, unchosenAssQuestionId, answerId, time) {
    return db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                return user['id'];
            }
        })
        .then(function (userId) {
            return db.getExperimentForUser(experimentId, userId)
                .then(function (experiment) {
                    var courseId = experiment.courseId;
                    var quizId = experiment.quizId;
                    return canvas.getCourseQuizQuestion(courseId, quizId, chosenQuestionId)
                        .then(function (questionJson) {
                            var grade = gradeForAnswer(questionJson, [answerId]);
                            var subjectId = canvas.questionSubjectId(questionJson.question_name);
                            return db.insertSubmissionSQL(experimentId, userId, chosenAssQuestionId, unchosenAssQuestionId, subjectId, answerId, time, grade)
                                .then(function () {
                                    return grade;
                                })
                        })
                })
        })
}

function gradeForAnswer(questionJson, answers) {
    if (questionJson.question_type == "short_answer_question") {
        return gradeForAnswerByField(questionJson, answers, "text");
    } else if (questionJson.question_type == "multiple_choice_question") {
        return gradeForAnswerByField(questionJson, answers, "id");
    }
    return 100;
}

function gradeForAnswerByField(questionJson, answerIds, answerField) {
    var grade = 0;
    questionJson.answers.forEach(function (answer) {
        if (answerIds.indexOf(answer[answerField]) >= 0) {
            grade += answer.weight;
        }
    });
    return grade;
}


module.exports = router;