var db = require('../db')
var express = require('express');
var router = express.Router();
var canvas = require("../canvas");
var Promise = require('promise');

/* GET users listing. */
router.get('/', function (req, res) {
    getExperimentsStats()
        .then(function (experiments) {
            res.json(experiments);
        })
});

function getExperimentsStats() {
    return db.getExperiments()
        .then(function (experiments) {
            return Promise.all(experiments.map(function(experiment) {
                return canvas.getQuizStatistics(experiment.courseId, experiment.quizId)
                    .then(function (stats) {
                        return {
                            name: experiment.name,
                            url: stats.html_url
                        }
                    })
            })).then(function(res) {
                return {
                    experiments_stats: res
                }
            })
        })
}

module.exports = router;