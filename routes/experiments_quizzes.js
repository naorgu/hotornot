var db = require('../db')
var express = require('express');
var router = express.Router();
var recommendations = require("../recommendation");
var canvas = require("../canvas");

/* GET users listing. */
router.get('/', function (req, res) {
    var courseId = req.param("courseId");
    getCourseQuizzes(courseId)
        .then(function (quizzes) {
            res.json(quizzes);
        })
});

function getCourseQuizzes(courseId) {
    return canvas.getCourseQuizzes(courseId)
        .then(function (quizzes) {
            console.log(quizzes);
            return {
                quizzes: quizzes
            };
        })
}

module.exports = router;
