var db = require('../db');
var canvas = require('../canvas');
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    var token = req.cookies.auth;
    var experimentId = req.param("experimentId");
    db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                var userId = user['id'];
                return postQuizSubmission(token, userId, experimentId);
            } else {
                res.redirect('quizzes');
            }
        });
});

function postQuizSubmission(token, userId, experimentId) {
    console.log(userId + " " + experimentId);
    return db.getExperimentForUser(experimentId, userId)
        .then(function (experiment) {
            var courseId = courseId;
            var quizId = quizId;

            return canvas.getter('postQuizSubmission', {
                course_id: courseId,
                quiz_id: quizId
            }, token, {})
        })
}

module.exports = router;
