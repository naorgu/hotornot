/**
 * Created by naorguetta on 11/4/14.
 */
var db = require('../db')
var canvas = require('../canvas')
var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');


router.post('/', function (req, res, next) {
    var experimentJson = req.body;
    var token = req.cookies.auth;

    insertExperiment(token, experimentJson)
        .then(function () {
            res.send('ok')
        });
});


function insertExperiment(token, experimentJson) {
    var distribution = experimentJson.groups;
    var name = experimentJson.name;
    var description = experimentJson.description;
    var courseId = experimentJson.courseId;
    var quizId = experimentJson.quizId;
    var numQuestions = experimentJson.questionsNumber;

    return canvas.getCourseUsers(courseId)
        .then(function (usersJson) {
            var userIdsList = usersJson.map(function (x) { return x.id });
            var groups = userGroupsByPercentage(distribution, userIdsList);

            return db.insertExperiment({
                name: name,
                description: description,
                courseId: courseId,
                quizId: quizId,
                groups: groups,
                numQuestions: numQuestions
            })
        })
}

function userGroupsByPercentage(distribution, userIdsList) {
    var ans = [];
    var numUsers = userIdsList.length;

    var start = 0;
    for (var i = 0; i < distribution.length; i++) {
        var group = distribution[i];
        var recommender = group.recommender;

        var end;
        if (i < distribution.length - 1) {
            end = Math.floor(start + numUsers * group.percentage / 100);
        } else {
            end = numUsers;
        }
        var users = userIdsList.slice(start, end);
        start = end;

        ans.push({
            recommender: recommender,
            users: users
        })
    }
    return ans;
}


module.exports = router;