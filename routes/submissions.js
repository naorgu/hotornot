/**
 * Created by naorguetta on 11/4/14.
 */
var db = require('../db')
var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var canvas = require("../canvas");

router.post('/', function (req, res, next) {
    var token = req.cookies.auth;
    var body = req.body;
    var choseQuestionId = body.chosen_question_id;
    var answerId = body.answer;
    var experimentId = body.experimentId;
    var time = body.time;

    insertSubmission(token, experimentId, choseQuestionId, answerId, time).then(res.send('ok'));
});

function insertSubmission(token, experimentId, chosenQuestionId, answerId, time) {
    return db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                return user['id'];
            }
        })
        .then(function (userId) {
            return db.getExperimentForUser(experimentId, userId)
                .then(function (experiment) {
                    return gradeForAnswer(experiment.courseId, experiment.quizId,
                        chosenQuestionId, [answerId])
                })
                .then(function (grade) {
                    db.insertSubmissionSQL(experimentId, userId, chosenQuestionId, time, grade);
                })
        })
}

function gradeForAnswer(courseId, quizId, questionId, answerIds) {
    return canvas.getter('getCourseQuizQuestion', {
        course_id: courseId,
        quiz_id: quizId,
        question_id: questionId
    }, token)
        .then(function (question) {
            console.log(question);
            var grade = 0;
            question.answers.forEach(function (answer) {
                if (answerIds.indexOf(answer.id) >= 0) {
                    grade += answer.weight;
                }
            })
            return grade;
        })
}


module.exports = router;