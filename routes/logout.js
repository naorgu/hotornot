var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var config = require('../config/config.json');

router.get('/', function (req, res) {
    res.header('Set-Cookie', "auth=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT");
    res.redirect(config.canvas.url + "/logout/" + "?redirect_uri=" + config.hotOrNot.url );
});

module.exports = router;