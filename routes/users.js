var db = require('../db')
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    db.getUserByToken(req.cookies.auth)
        .then(function (user) {
            if (user != null) {
                res.json({
                    name: user.name,
                    isAdmin: user.isAdmin
                });
            } else {
                res.json({});
            }
        });
});


module.exports = router;
