var Promise = require('promise');
var Client = require('canvas-lms.js')
var config = require('./config/config.json').canvas
var request = require("request");


var closure = {
    oAuth: function (code) {
        return new Promise(function (fullfil, reject) {
            request.post({
                url: config.url + "/login/oauth2/token",
                method: 'POST',
                rejectUnauthorized: false,
                form: {
                    'client_id': config.clientId,
                    'client_secret': config.clientSecret,
                    'code': code
                }
            }, function (error, response, body) {
                fullfil(body);
            });
        })
    },
    getter: function (method, args, token, jsonObj) {
        var tokenToUse = (token ? token : config.adminToken);
        return getSession(tokenToUse)
            .then(function (canvas) {
                return canvas[method](args, jsonObj)
            })
            .then(function (response) {
                return response.toOption().getOrElse();
            })
    },
    createOrGetQuizSubmission: function (courseId, quizId, token) {
        return createOrGetQuizSubmission(courseId, quizId, token);
    },
    getSubmissionQuestions: function (submissionId, token) {
        return getter('getQuizSubmissionQuestions', {quiz_submission_id: submissionId}, token)
            .then(function (ans) {
                return ans.quiz_submission_questions;
            })
    },
    getQuizSubmissions: function (courseId, quizId) {
        return getter('getQuizSubmissions', {course_id: courseId, quiz_id: quizId})
            .then(function (submissions) {
                return submissions.quiz_submissions;
            })
    },
    getQuizStatistics: function (courseId, quizId) {
        return getter('getQuizStatistics', {course_id: courseId, quiz_id: quizId})
            .then(function (ans) {
                return ans.quiz_statistics[0];
            })
    },
    getCourseUsers: function (courseId) {
        return getter('getCourseUsers', {course_id: courseId})
            .then(function (ans) {
                return ans;
            })
        {
        }
    },
    getCourses: function () {
        return getter('getCourses')
            .then(function (ans) {
                return ans;
            })
    },
    getCourseQuizzes: function (courseId) {
        return getter('getCourseQuizzes', {course_id: courseId})
            .then(function (ans) {
                return ans;
            })
    },
    completeQuizSubmission: function (courseId, quizId, token) {
        return getter('getQuizSubmissions', {
            course_id: courseId,
            quiz_id: quizId
        }, token)
            .then(function (submissions) {
                var submission = submissions.quiz_submissions[0];
                var completeQuizSubmission = {
                    attempt: 1,
                    validation_token: submission.validation_token
                }
                return getter('postCompleteQuizSubmission', {
                        course_id: courseId,
                        quiz_id: quizId,
                        submission_id: submission.id
                    },
                    token, completeQuizSubmission)
            })
    },
    answerQuizSubmissionQuestion: function (courseId, quizId, chosenQuestionId, answerId, token) {
        return createOrGetQuizSubmission(courseId, quizId, token)
            .then(function (submission) {
                var postSubmissionQuestion = {
                    attempt: 1,
                    validation_token: submission.validation_token,
                    quiz_questions: [{
                        id: chosenQuestionId,
                        answer: answerId
                    }]
                }
                return getter('postQuizSubmissionQuestion',
                    {quiz_submission_id: submission.id}, token, postSubmissionQuestion)
            })
    },
    getCourseQuizQuestion: function (courseId, quizId, questionId) {
        return getter('getCourseQuizQuestion', {
            course_id: courseId,
            quiz_id: quizId,
            question_id: questionId
        })
    },
    isAdmin: function (userId) {
        return getter('getAccountAdmins', {account_id: 1})
            .then(function (admins) {
                console.log(admins);
                for (var i = 0; i < admins.length; i++) {
                    if (admins[i].user.id == userId) {
                        return true;
                    }
                }
                return false;
            })
    },
    questionSubjectId: function (questionName) {
        var res = questionName.split(":");
        return parseInt(res[0]);
    },
    questionLevel: function (questionName) {
        var res = questionName.split(":");
        return parseInt(res[res.length - 1]);
    }
}

function getSession(token) {
    return new Promise(function (fullfil, reject) {
        var client = Client.client(config.url, token);
        client.withSession(function (canvas) {
            fullfil(canvas);
        })
    })
}

function getter(method, args, token, jsonObj) {
    var tokenToUse = (token ? token : config.adminToken);
    return getSession(tokenToUse)
        .then(function (canvas) {
            return canvas[method](args, jsonObj)
        })
        .then(function (response) {
            return response.toOption().getOrElse();
        })
}

function createOrGetQuizSubmission(courseId, quizId, token) {
    return getter('getQuizSubmissions', {
        course_id: courseId,
        quiz_id: quizId
    }, token)
        .then(function (res) {
            var submissions = res.quiz_submissions;
            if (submissions.length > 0) {
                return submissions[0];
            } else {
                return getter('postQuizSubmission', {
                    course_id: courseId,
                    quiz_id: quizId
                }, token, {})
                    .then(function (res) {
                        return res.quiz_submissions[0];
                    })
            }
        })
}

module.exports = closure;