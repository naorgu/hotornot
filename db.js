var models = require('./models')
var Promise = require('promise')

var mongoModels = require('./mongo/models')

var closure = {
    insertUser: function (userId, userName) {
        return models.User.create({
            "id": userId,
            "name": userName
        })
    },
    insertToken: function (accessToken, userId) {
        return models.AccessToken.create({
            "access_token": accessToken,
            "UserId": userId
        }).then(null, function (rej) {
        })
    },
    getUserByToken: function (accessToken) {
        return models.AccessToken.findOne({
            where: {
                'access_token': accessToken
            },
            include: [models.User]
        }).then(function (res) {
            if (res != null) {
                return res.User.dataValues;
            } else {
                return {};
            }
        });
    },
    getUserList: function () {
        return models.User.findAll()
            .then(function (users) {
                return users.map(function (x) {
                    return x.dataValues.id
                })
            })
    },
    insertQuestion: function (questionId) {
        return models.Question.create({
            "id": questionId
        }).then(null, function (rej) {
        })
    },
    insertSubmission: function (userId, questionId, grade) {
        return models.Submission.create({
            "UserId": userId,
            "QuestionId": questionId,
            "grade": grade
        }).then(null, function (rej) {
        })
    },
    insertExperiment: function (name, courseId, description) {
        return models.Experiment.create({
            "name": name,
            "description": description,
            "CourseId": courseId
        }).then(function (res) {
            return res.dataValues.id;
        }, function (rej) {
        })
    },
    insertExperimentGroup: function (experimentId, recommender, userIds) {
        return models.ExperimentGroup.create({
            "ExperimentId": experimentId,
            "recommender": recommender
        }).then(function (res) {
            var records = [];
            userIds.forEach(function (userId) {
                records.push({
                    "UserId": userId,
                    "ExperimentGroupId": res.dataValues.id
                })
            })
            models.ExperimentGroupUser.bulkCreate(records)
                .then(null, function (rej) {
                })
        })
    },
    getRecommenderByUser: function (userId, experimentId) {
        return models.User.findOne({
            where: {
                'id': userId
            },
            include: [models.ExperimentGroup]
        }).then(function (res) {
            res.dataValues.ExperimentGroups.forEach(function (expGroup) {
                var obj = expGroup.dataValues;
                if (obj.ExperimentId == experimentId) {
                    console.log(obj.recommender);
                    return obj.recommender;
                }
                return null;
            })
        });
    },
    getQuestionsAnswered: function (userId) {
        return models.Submission.findAll({
            where: {
                'UserId': userId
            },
            attributes: ['QuestionId']
        }).then(function (res) {
            var ans = [];
            res.forEach(function (x) {
                questionId = x.dataValues.QuestionId;
                if (ans.indexOf(questionId) < 0) {
                    ans.push(questionId);
                }
            });
            return ans;
        });
    }
}

var mongoClosure = {
    insertUser: function (userId, userName, isAdmin, token) {
        var update = {_id: userId, name: userName, isAdmin: isAdmin, $push: {tokens: token}};
        var options = {upsert: true};
        return mongoModels.User.findOneAndUpdate({_id: userId}, update, options, function (err, person) {
            console.log(err);
            console.log(person);
        });
    },
    getUserByToken: function (token) {
        return mongoModels.User.findOne({
            tokens: token
        })
            .exec()
            .then(function (res) {
                return res;
            })
    },
    insertExperiment: function (experimentJson) {
        var groups = [];
        experimentJson.groups.forEach(function (group) {
            groups.push({
                recommender: group.recommender,
                users: group.users
            });
        })
        return new mongoModels.Experiment({
            name: experimentJson.name,
            description: experimentJson.description,
            courseId: experimentJson.courseId,
            quizId: experimentJson.quizId,
            numQuestions: experimentJson.numQuestions,
            groups: groups
        }).save(function (err) {
                console.log(err);
            });
    },
    getExperimentsForUser: function (userId) {
        return mongoModels.Experiment.find({
            "groups.users": userId
        })
            .exec()
            .then(function (experiments) {
                return {
                    experiments: experiments.map(function (experiment) {
                        return {
                            id: experiment._id,
                            name: experiment.name
                        }
                    })
                }
            })
    },
    getExperimentForUser: function (experimentId, userId) {
        return mongoModels.Experiment.findOne({
            _id: experimentId
        })
            .exec()
            .then(function (experiment) {
                var recommender = 0;
                experiment.groups.forEach(function (group) {
                    var i = group.users.indexOf(userId);
                    if (i >= 0) {
                        recommender = group.recommender;
                    }
                })
                return {
                    name: experiment.name,
                    description: experiment.description,
                    courseId: experiment.courseId,
                    quizId: experiment.quizId,
                    numQuestions: experiment.numQuestions,
                    recommender: recommender
                };
            })
    },
    getExperiment: function (experimentId) {
        return mongoModels.Experiment.findOne({
            _id: experimentId
        })
            .exec()
    },
    getExperiments: function () {
        return mongoModels.Experiment.find()
            .exec()
            .then(function (experiments) {
                return experiments.map(function (experiment) {
                    return {
                        name: experiment.name,
                        description: experiment.description,
                        courseId: experiment.courseId,
                        quizId: experiment.quizId
                    }
                })
            })
    },
    getRecommenderForUser: function (experimentId, userId) {
        return mongoModels.Experiment.findOne({
            _id: experimentId
        })
            .exec()
            .then(function (experiment) {
                var recommender = 0;
                experiment.groups.forEach(function (group) {
                    var i = group.users.indexOf(userId);
                    if (i >= 0) {
                        recommender = group.recommender;
                    }
                })
                return recommender;
            }, function (err) { console.log(err); })
    },
    insertQuestions: function (experimentId, userId, questions) {
        return new mongoModels.Submission({
            experimentId: experimentId,
            userId: userId,
            questions: questions
        }).save(function (err) {
                console.log(err);
            });
    },
    getQuestions: function (experimentId, userId) {
        return mongoModels.Submission.findOne({
            experimentId: experimentId,
            userId: userId
        })
            .exec()
            .then(function (res) {
                if (res != null) {
                    return res.questions;
                }
                return null;
            })
    },
    getQuestionsAnswered: function (experimentId, userId) {
        return models.Submission.findAll({
            where: {
                'ExperimentId': experimentId,
                'UserId': userId
            },
            attributes: ['QuestionId']
        }).then(function (res) {
            var ans = [];
            res.forEach(function (x) {
                questionId = x.dataValues.QuestionId;
                if (ans.indexOf(questionId) < 0) {
                    ans.push(questionId);
                }
            });
            return ans;
        });
    },
    insertSubmissionSQL: function (experimentId, userId, chosenQuestionId, ignoredQuestionId, subjectId, answer, time, grade) {
        return models.Submission.create({
            "ExperimentId": experimentId,
            "UserId": userId,
            "ChosenQuestionId": chosenQuestionId,
            "IgnoredQuestionId": ignoredQuestionId,
            "SubjectId": subjectId,
            "Answer": answer,
            "time": time,
            "grade": grade
        }).then(null, function (rej) {
            console.log(rej);
        })
    },
    getUsersAverage: function (experimentId) {
        return models.sequelize.query('select "UserId", AVG(grade) from "Submissions" GROUP BY "UserId"',
            {type: models.sequelize.QueryTypes.SELECT})
            .then(function (userAverages) {
                return userAverages;
            })
    },
    getUsersGrades: function (userIds) {
        return models.Submission.findAll({
            where: {
                'UserId': {
                    in: userIds
                }
            },
            attributes: ['UserId', 'QuestionId', 'grade']
        }).then(function (res) {
            var ans = [];
            userIds.forEach(function (userId) {
                var element = [];
                res.forEach(function (x) {
                    var data = x.dataValues;
                    if (userId == data.UserId) {
                        element.push({questionId: data.QuestionId, grade: data.grade});
                    }
                })
                ans.push({userId: userId, grades: element});
            })
            return ans;
        });
    },
    removeQuestionsAnswered: function (userId) {
        return models.Submission.destroy({
            where: {
                'UserId': userId
            }
        }).then(function (res) {
            return;
        }, function (rej) {
        })
    }
}

module.exports = mongoClosure;